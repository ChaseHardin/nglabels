/// <reference path="../scripts/typings/angularjs/angular.d.ts" />
var LabelApplication;
(function (LabelApplication) {
    var LabelCollection = (function () {
        function LabelCollection($scope, service) {
            this.$scope = $scope;
            this.service = service;
            this.sequence = service.retrieveAllLabels();
        }
        LabelCollection.prototype.update = function (label) {
            this.service.updateLabel(label);
        };
        return LabelCollection;
    }());
    LabelApplication.LabelCollection = LabelCollection;
    LabelApplication.LabelEditor
        .editorModule
        .controller("labelCollection", ["$scope", "labelDataService", LabelCollection]);
})(LabelApplication || (LabelApplication = {}));
//# sourceMappingURL=LabelCollection.js.map