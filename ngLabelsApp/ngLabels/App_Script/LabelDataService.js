/// <reference path="label.d.ts" />
/// <reference path="../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../scripts/typings/angularjs/angular-resource.d.ts" />
var LabelApplication;
(function (LabelApplication) {
    LabelApplication.LabelEditor.editorModule.factory('labelDataService', ["$resource",
        function (r) {
            return new LabelDataService(r);
        }]);
    var LabelDataService = (function () {
        function LabelDataService($resource) {
            this.resource = $resource("api/Labels/:id", { id: "@id" }, {
                get: { method: "GET" },
                save: { method: "PUT" },
                query: { method: "GET", isArray: true },
                create: { method: "POST" },
                delete: { method: "DELETE" }
            });
        }
        LabelDataService.prototype.retrieveAllLabels = function () {
            return this.resource.query();
        };
        LabelDataService.prototype.updateLabel = function (label) {
            this.resource.save({ id: label.Id }, label);
        };
        return LabelDataService;
    }());
    LabelApplication.LabelDataService = LabelDataService;
})(LabelApplication || (LabelApplication = {}));
;
//# sourceMappingURL=LabelDataService.js.map